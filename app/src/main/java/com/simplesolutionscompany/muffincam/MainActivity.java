package com.simplesolutionscompany.muffincam;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public static final String VIDEO_CAPTURED = "com.simlesolutionscompany.muffincam.VIDEO_CAPTURED";
    public static final String VIDEO_CAPTURE_FAILED = "com.simlesolutionscompany.muffincam.VIDEO_CAPTURE_FAILED";
    public static final String INTENT_EXTRA_LOCAL_VIDEOPATH = "com.simplesolutionscompany.muffincam.LOCAL_VIDEO_PATH";
    public static final String VIDEO_UPLOADED = "com.simlesolutionscompany.muffincam.VIDEO_UPLOADED";
    public static final String VIDEO_UPLOAD_FAILED = "com.simlesolutionscompany.muffincam.UPLOAD_FAILED";
    public static final String INTENT_EXTRA_DROPBOX_VIDEOPATH = "com.simplesolutionscompany.muffincam.DROPBOX_VIDEO_PATH";
    public static final int SHOOTING_INTERVAL = 3600; // 1 hour

    public static final String SHARED_PREF_NAME = "MuffinCam";

    private PendingIntent mLastAlarmPendingIntent = null;
    private boolean mNextShootScheduled = false;

    private BroadcastReceiver localBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MainActivity.VIDEO_CAPTURED)) {
                // handle video?
                TextView textView = (TextView)findViewById(R.id.helloworld);
                String videoFilePath = intent.getStringExtra(INTENT_EXTRA_LOCAL_VIDEOPATH);
                if (videoFilePath == null) {
                    videoFilePath = "Video file path not received";
                }
                textView.setText(videoFilePath);

                Intent i = new Intent(getBaseContext(), DropboxRegistrationActivity.class);
                i.putExtra(DropboxRegistrationActivity.DROP_BOX_REQUEST_FIELD, DropboxRegistrationActivity.DROPBOX_UPLOAD_REQUEST_CODE);
                i.putExtra(DropboxRegistrationActivity.DROP_BOX_UPLOAD_FIELD, videoFilePath);
                startActivityForResult(i, DropboxRegistrationActivity.DROPBOX_UPLOAD_REQUEST_CODE);

            }
            else if (intent.getAction().equals(MainActivity.VIDEO_CAPTURE_FAILED)) {
                TextView textView = (TextView)findViewById(R.id.helloworld);
                textView.setText(R.string.video_capture_failed);
                scheduleNextShoot(SHOOTING_INTERVAL);
            }
        }
    };

    // only report WiFi status, if any
    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

            TextView networkstateView = (TextView) findViewById(R.id.networkstate_display);
            NetworkInfo activeNetInfo = connMgr.getActiveNetworkInfo();
            if (activeNetInfo == null) {
                networkstateView.setText("No active networks");
            } else {
                if (activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // reflect in label
                    switch (activeNetInfo.getState()) {
                        case DISCONNECTED:
                            networkstateView.setText("Disconnected");
                            break;
                        case DISCONNECTING:
                            networkstateView.setText("Disconnecting...");
                            break;
                        case CONNECTED:
                            networkstateView.setText("Connected");
                            break;
                        case CONNECTING:
                            networkstateView.setText("Connecting...");
                            break;
                        case SUSPENDED:
                            networkstateView.setText("Suspended(?)");
                            break;
                        case UNKNOWN:
                            networkstateView.setText("Unknown");
                            break;
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
        IntentFilter intentFilter = new IntentFilter(MainActivity.VIDEO_CAPTURED);
        intentFilter.addAction(MainActivity.VIDEO_CAPTURE_FAILED);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        lbm.registerReceiver(localBroadcastReceiver, intentFilter);

        // Register for network-change events
        registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        if (!dropBoxTokenExists()) {
            Intent i = new Intent(this, DropboxRegistrationActivity.class);
            i.putExtra(DropboxRegistrationActivity.DROP_BOX_REQUEST_FIELD, DropboxRegistrationActivity.DROPBOX_AUTH_REQUEST_CODE);

            startActivityForResult(i, DropboxRegistrationActivity.DROPBOX_AUTH_REQUEST_CODE);
        }
        else {
            String token = loadDropBoxToken(getBaseContext());
            displayDropboxToken(token);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mNetworkBroadcastReceiver);

    }

    @Override
    protected void onResume() {
        Button wifiButton;

        super.onResume();
        WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        switch (wifiManager.getWifiState()) {
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_DISABLING:
                wifiButton = (Button)findViewById(R.id.wifi_on_off_button);
                wifiButton.setText(R.string.wifi_on);
                break;
            case WifiManager.WIFI_STATE_ENABLED:
            case WifiManager.WIFI_STATE_ENABLING:
                wifiButton = (Button)findViewById(R.id.wifi_on_off_button);
                wifiButton.setText(R.string.wifi_off);
                break;
            case WifiManager.WIFI_STATE_UNKNOWN:
                wifiButton = (Button)findViewById(R.id.wifi_on_off_button);
                wifiButton.setText(R.string.wifi_on);
                Log.w(TAG, "WiFi state is reported as UNKNOWN!!!");
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void scheduleNextShoot(int seconds) {

        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent();
        intent.setClass(getBaseContext(), CameraActivity.class);
        intent.setAction(Intent.ACTION_MAIN);
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + seconds * 1000,
                pendingIntent);

        mLastAlarmPendingIntent = pendingIntent;
    }

    public void onButtonClicked(View view) {

        if (mNextShootScheduled) {
            if (mLastAlarmPendingIntent != null) {
                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                alarmManager.cancel(mLastAlarmPendingIntent);
            }
        }
        else {
            scheduleNextShoot(SHOOTING_INTERVAL);
        }
        mNextShootScheduled = !mNextShootScheduled;
        Button b = (Button)findViewById(R.id.start_stop_button);
        b.setText(mNextShootScheduled? R.string.stop_alarm : R.string.start_alarm);

    }


    // TBD: monitor wifi state with broadcast receiver for real-time update of label state
    public void onWiFiButtonClicked(View view) {

        Button wifiButton = (Button)findViewById(R.id.wifi_on_off_button);


        WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        switch (wifiManager.getWifiState()) {
            case WifiManager.WIFI_STATE_DISABLED:
            case WifiManager.WIFI_STATE_DISABLING:
                wifiManager.setWifiEnabled(true);
                wifiButton.setText(R.string.wifi_off);
                break;

            case WifiManager.WIFI_STATE_ENABLED:
            case WifiManager.WIFI_STATE_ENABLING:
                wifiManager.setWifiEnabled(false);
                wifiButton.setText(R.string.wifi_on);
                break;

            case WifiManager.WIFI_STATE_UNKNOWN:
                wifiManager.setWifiEnabled(false);
                wifiButton.setText(R.string.wifi_off);
                Log.w(TAG, "WiFi state is reported as UNKNOWN!!!");
                break;
        }

    }

    public void onClearTokenButtonClicked(View view) {

        SharedPreferences sPref = getBaseContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        if (sPref.contains(DropboxRegistrationActivity.OATH_TOKEN_FIELD)) {
            SharedPreferences.Editor sPrefEditor = sPref.edit();
            sPrefEditor.remove(DropboxRegistrationActivity.OATH_TOKEN_FIELD);
            sPrefEditor.commit();
            Log.d(TAG, "Dropbox token removed");
        }
        else {
            Log.d(TAG, "Dropbox token not found");
        }

    }

    private boolean dropBoxTokenExists() {

        SharedPreferences sPref = getBaseContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        return sPref.contains(DropboxRegistrationActivity.OATH_TOKEN_FIELD);
    }

    public static String loadDropBoxToken(Context context) {

        String token = null;

        SharedPreferences sPref = context.getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        if (sPref.contains(DropboxRegistrationActivity.OATH_TOKEN_FIELD)) {
            token = sPref.getString(DropboxRegistrationActivity.OATH_TOKEN_FIELD, null);
        }
        return token;
    }

    private boolean saveDropBoxToken(String token) {

        SharedPreferences sPref = getBaseContext().getSharedPreferences(SHARED_PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor sPrefEditor = sPref.edit();
        sPrefEditor.putString(DropboxRegistrationActivity.OATH_TOKEN_FIELD, token);
        sPrefEditor.commit();

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DropboxRegistrationActivity.DROPBOX_AUTH_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // token obtained
                String token = data.getStringExtra(DropboxRegistrationActivity.OATH_TOKEN_FIELD);

                // save to shared preference
                saveDropBoxToken(token);

                displayDropboxToken(token);
            }
        }
        else if (requestCode == DropboxRegistrationActivity.DROPBOX_UPLOAD_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // display path
                String dropboxPath = data.getStringExtra(MainActivity.INTENT_EXTRA_DROPBOX_VIDEOPATH);
                TextView t = (TextView)findViewById(R.id.file_upload_status);
                t.setText("File uploaded: " + dropboxPath);
            }
            else {
                // display error message?
                TextView t = (TextView)findViewById(R.id.file_upload_status);
                t.setText("File upload failed");
            }

            scheduleNextShoot(SHOOTING_INTERVAL);
        }
    }

    private void displayDropboxToken(String token) {
        TextView t = ((TextView)findViewById(R.id.token_display));
        if (token != null) {
            t.setText(token);
        }
        else {
            t.setText("Not available");
        }
    }
}

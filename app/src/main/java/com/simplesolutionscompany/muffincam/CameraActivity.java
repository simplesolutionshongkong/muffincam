package com.simplesolutionscompany.muffincam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import java.io.File;
import java.io.IOException;

public class CameraActivity extends AppCompatActivity implements Camera.ErrorCallback, Handler.Callback {

    public static CamcorderProfile RecorderProfile;

    private static final String TAG = "CameraActivity";
    private static final String VideoSubDir = "MuffinCam";
    private static final String VideoBasename = "Muffin";
    private static final long VideoLength = 180; // seconds

    private CameraPreview mPreview;
    private Camera mCamera;
    private MediaRecorder mRecorder;
    private String mLastOutputFile;
    private Handler mHandler;
    private OpenCameraTask mOpenCameraTask;
    private StopRecorderTask mStopRecorderTask;
    private VideoFileObserver mVideoFileObserver;


    static {
        int desiredQuality = CamcorderProfile.QUALITY_480P;
        if (CamcorderProfile.hasProfile(desiredQuality)) {
            RecorderProfile = CamcorderProfile.get(desiredQuality);
        }
        else {
            /* find first back-facing camera (debug only)
            int nCams = Camera.getNumberOfCameras();
            for (int i = 0; i < nCams; i++){
                Camera.CameraInfo camInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(i, camInfo);
                Log.d(TAG, "Camera " + i + ": " + "facing " + camInfo.facing + ", orient " + camInfo.orientation);

            }
            */
            Log.w(TAG, "desired profile not available, choosing the highest available resolution");
            RecorderProfile = CamcorderProfile.get(0, CamcorderProfile.QUALITY_HIGH);
            if (RecorderProfile.videoFrameWidth < 640) {
                Log.d(TAG, "Overriding frame-width from " + RecorderProfile.videoFrameWidth + " to 640");
                RecorderProfile.videoFrameWidth = 640;
            }
            if (RecorderProfile.videoFrameHeight < 480) {
                Log.d(TAG, "Overriding frame-height from " + RecorderProfile.videoFrameHeight + " to 480");
                RecorderProfile.videoFrameHeight = 480;
            }
            if (RecorderProfile.videoFrameRate < 25) {
                Log.d(TAG, "Overriding frame-rate from " + RecorderProfile.videoFrameRate + " to 25");
                RecorderProfile.videoFrameRate = 25;
            }
            // YouTube recommends ~ 2.5 Mbps for 480p video (standard frame-rates)
            int desiredVBR = 3 * 1024 * 1024;
            if (RecorderProfile.videoBitRate < desiredVBR) {
                Log.d(TAG, "Overriding video bit-rate from " + RecorderProfile.videoBitRate + " to " + desiredVBR);
                RecorderProfile.videoBitRate = desiredVBR;
            }
        }
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CameraPreview.SURFACE_PREVIEW_READY)) {
                // Configure MediaRecorder and Start Record
                if (prepareVideoRecorder()) {
                    mRecorder.start();
                    mHandler.sendEmptyMessageDelayed(1, VideoLength * 1000);
                }
                else {
                    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
                    lbm.sendBroadcast(new Intent(MainActivity.VIDEO_CAPTURE_FAILED));
                    finish();
                }
            }
            else if (intent.getAction().equals(CameraPreview.SURFACE_PREVIEW_FAILED)) {
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
                lbm.sendBroadcast(new Intent(MainActivity.VIDEO_CAPTURE_FAILED));
                finish();
            }
        }
    };

    @Override
    public void onError(int i, Camera camera) {

        switch (i) {
            case Camera.CAMERA_ERROR_UNKNOWN:
                Log.e(TAG, "Camera-Error: Unknown");
                break;
            case Camera.CAMERA_ERROR_EVICTED:
                Log.e(TAG, "Camera-Error: Evicted");
                break;
            case Camera.CAMERA_ERROR_SERVER_DIED:
                Log.e(TAG, "Camera-Error: Server Died. Need restart.");
                break;
        }
    }

    private class VideoFileObserver extends FileObserver {


        public VideoFileObserver(String file) {
            super(file);
        }

        @Override
        public void onEvent(int i, String s) {
            // run from another thread, use message/Handler to pass control back to 'UI thread'
            if (i == CLOSE_WRITE) {
                Log.d(TAG, "VideoFile observed Closed for Writing. " + s);
                mHandler.sendEmptyMessage(2);

            }
            else {
                // ignore other events
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
        IntentFilter intentFilter = new IntentFilter(CameraPreview.SURFACE_PREVIEW_READY);
        intentFilter.addAction(CameraPreview.SURFACE_PREVIEW_FAILED);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        lbm.registerReceiver(mBroadcastReceiver, intentFilter);

        mHandler = new Handler(this);

        mOpenCameraTask = new OpenCameraTask();
        mOpenCameraTask.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
        lbm.unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    }

    private boolean prepareVideoRecorder() {

        boolean succ = false;
        try {
            mCamera.setErrorCallback(this);
            setFocusModeToInfinity(); // before unlock
            mCamera.unlock();
            mRecorder = new MediaRecorder();
            mRecorder.setCamera(mCamera);
            mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
            mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mRecorder.setProfile(CameraActivity.RecorderProfile);
            mLastOutputFile = composeVideoOutputFilePath();
            mRecorder.setOutputFile(mLastOutputFile);
            mRecorder.setPreviewDisplay(mPreview.getHolder().getSurface());
            mRecorder.prepare();
            succ = true;

        }
        catch (RuntimeException re) {
            Log.e(TAG, "Recorder preparation error: " + re.getMessage());
        }
        catch (IOException ioe) {
            Log.e(TAG, "Recorder preparation error: " + ioe.getMessage());
        }

        return succ;
    }

    private void setFocusModeToInfinity() {

        Camera.Parameters params = mCamera.getParameters();
        String currFocusMode = params.getFocusMode();
        if (! currFocusMode.equals(Camera.Parameters.FOCUS_MODE_INFINITY)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
            mCamera.setParameters(params);
            Log.d(TAG, "Camera Focus Mode changed from " + currFocusMode + " to " + params.FOCUS_MODE_INFINITY);
        }
    }

    private String composeVideoOutputFilePath() {

        File videoPath = null;

        File mediaStorageDir = new File (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES),
                VideoSubDir);
        if (! mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e(TAG, "Cannot create directory, " + mediaStorageDir + ".");
                return null;
            }
        }
        // overwrite the last one
        // try removing before writing, reduce changes output file error (cannot play)
        //
        videoPath = new File(mediaStorageDir.getPath() + File.separator +
                VideoBasename + ".mp4");

        if (videoPath.exists()) {
            if (! videoPath.delete()) {
                Log.e(TAG, "Error deleting video file, " + videoPath.getPath());
            }
            else {
                Log.d(TAG, "Previous video file deleted.");
            }
        }
        return videoPath.getPath();
    }

    @Override
    public boolean handleMessage(Message message) {

        switch (message.what) {
            case 1:
                mVideoFileObserver = new VideoFileObserver(mLastOutputFile);
                mVideoFileObserver.startWatching();
                /* try { */
                    // see CameraRecorder example:
                    // https://github.com/pickerweng/CameraRecorder/blob/master/src/com/android/camerarecorder/RecorderService.java
                    // also see Camera API reference
                    // mCamera.reconnect();
                    // stop camera
                    Log.d(TAG, "Stopping Recorder...");
                    mRecorder.stop();
                /* } */
                /*
                catch (IOException ioe) {
                    Log.e(TAG, "Camera.reconnect() error. " + ioe.getMessage());
                }
                */
                // wait till file stopped writing before cleaning up recorder and camera instances

                break;
            case 2:
                mVideoFileObserver.stopWatching();
                mStopRecorderTask = new StopRecorderTask();
                mStopRecorderTask.execute();
                break;
        }


        return false;
    }

    private class OpenCameraTask extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected Boolean doInBackground(Void... voids) {
            Boolean cameraOpened = false;

            try {
                mCamera = Camera.open();
                cameraOpened = true;
            }
            catch (RuntimeException re) {
                Log.e(TAG, "Camera open failed: " + re.getMessage());
                cameraOpened = false;
            }

            return cameraOpened;
        }

        @Override
        protected void onPostExecute(Boolean cameraOpened) {
            if (cameraOpened) {
                // Add CameraPreview
                mPreview = new CameraPreview(getBaseContext(), mCamera);
                FrameLayout frameLayout = (FrameLayout)findViewById(R.id.camera_preview);
                frameLayout.addView(mPreview);
                // continue from CAMERA_PREVIEW_READY broadcast receiver
            }
            else {
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
                Intent intent = new Intent(MainActivity.VIDEO_CAPTURE_FAILED);
                lbm.sendBroadcast(intent);
                finish();
            }
        }
    }

    private class StopRecorderTask extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected Boolean doInBackground(Void... voids) {
            Boolean recorderStopped = false;

            Log.d(TAG, "Cleaning up Recorder...");
            try {
                // Recorder already stopped upon Handler received message
                // This is continuation after video file has been closed for writing
                // See if this could guard against corrupted output file that cannot be
                // opened for playback, by making sure that the instance variables are
                // reset after the output file is 'completed'
                //
                mRecorder.reset();


                mRecorder.release();

                mCamera.lock();

                mCamera.stopPreview();
                mCamera.release();



                recorderStopped = true;
            }
            /*
            catch (IOException ioe) {
                Log.e(TAG, "Camera.reconnect() error. " + ioe.getMessage());
            }
            */
            catch (Exception e) {
                recorderStopped = false;
            }

            return recorderStopped;
        }

        @Override
        protected void onPostExecute(Boolean recorderStopped) {
            if (recorderStopped) {
                Log.d(TAG, "mRecorder stopped");
                // notify main thread
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
                Intent intent = new Intent(MainActivity.VIDEO_CAPTURED);
                intent.putExtra(MainActivity.INTENT_EXTRA_LOCAL_VIDEOPATH, mLastOutputFile);
                lbm.sendBroadcast(intent);
                // close activity
                finish();
            }
            else {
                Log.e(TAG, "mRecorder stop failed");
                LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getBaseContext());
                Intent intent = new Intent(MainActivity.VIDEO_CAPTURE_FAILED);
                lbm.sendBroadcast(intent);
                finish();
            }
        }
    }
}

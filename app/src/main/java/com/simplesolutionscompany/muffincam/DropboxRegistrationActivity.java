package com.simplesolutionscompany.muffincam;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxFileSizeException;
import com.dropbox.client2.exception.DropboxIOException;
import com.dropbox.client2.exception.DropboxServerException;
import com.dropbox.client2.exception.DropboxUnlinkedException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Calendar;

public class DropboxRegistrationActivity extends AppCompatActivity implements Handler.Callback {

    public static final String DROP_BOX_REQUEST_FIELD = "DropBoxRequest";
    public static final String DROP_BOX_UPLOAD_FIELD = "DropBoxUploadFile"; // path of file to upload

    public static final int DROPBOX_AUTH_REQUEST_CODE = 100;
    public static final int DROPBOX_UPLOAD_REQUEST_CODE = 200;

    public static final String OATH_TOKEN_FIELD = "DropBox_OAuth_Token";

    public static final String APP_KEY = "wlt771xgfb067j9";
    public static final String APP_SECRET = "vzwxurvk2ab1fig";

    private static final String TAG = "DropboxRegActivity";

    private static final int NETWORK_WAIT_TIMER_MESSAGE = 1;
    private static final int NETWORK_WAIT_TIMER_SECONDS = 60;

    private static final int NETWORK_SETUP_FAILED_MESSAGE = 2;
    private static final int NETWORK_SETUP_COMPLETED_MESSAGE = 3;


    private Handler mHandler;

    private void beginDropboxRequest() {
        int requestType = getDropBoxRequest();
        switch (requestType) {
            case DROPBOX_AUTH_REQUEST_CODE:
                beginDropBoxAuthentication();
                break;
            case DROPBOX_UPLOAD_REQUEST_CODE:
                findViewById(R.id.reg_wait_network_progress).setVisibility(View.GONE);
                findViewById(R.id.reg_status_display).setVisibility(View.GONE);
                beginDropBoxUpload();
                break;
            default:
                break;
        }
    }


    @Override
    public boolean handleMessage(Message message) {
        switch (message.what) {
            case NETWORK_WAIT_TIMER_MESSAGE:
                if (isNetworkConnected()) {
                    beginDropboxRequest();
                }
                else {
                    setResult(RESULT_CANCELED);
                    finish();
                }
                break;
            case NETWORK_SETUP_FAILED_MESSAGE:
                setResult(Activity.RESULT_CANCELED);
                finish();
                break;
            case NETWORK_SETUP_COMPLETED_MESSAGE:
                beginDropboxRequest();
                break;
        }
        return false;
    }

    private enum DropBoxState {
        IDLE,
        WAIT_NETWORK, // Network Receiver
        WAIT_NETWORK_TIMER, // Timer
        AUTHENTICATING,
        AUTHENTICATED,
        UPLOADING,
        UPLOADED

    };

    private DropBoxState mState = DropBoxState.IDLE;

    public Boolean isNetworkConnected() {
        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connMgr.getActiveNetworkInfo();
        if (netInfo != null) {
            return netInfo.isConnected();
        }
        return false;
    }

    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {

        private boolean mFirstDisconnectReceived = false;

        @Override
        public void onReceive(Context context, Intent intent) {

            if (mState != DropBoxState.WAIT_NETWORK && mState != DropBoxState.WAIT_NETWORK_TIMER) {
                Log.w(TAG, "Ignore Network Activity Broadcast received while not waiting-for-network.");
                return;
            }
            ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wiFiNetInfo = connMgr.getActiveNetworkInfo();
            if (wiFiNetInfo == null) {
                wiFiNetInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            }
            // reflect in label
            switch (wiFiNetInfo.getState()) {
                case CONNECTING:
                    // keep waiting
                    Log.d(TAG, "WiFi connecting");
                    break;
                case CONNECTED:
                    mHandler.sendEmptyMessage(NETWORK_SETUP_COMPLETED_MESSAGE);
                    break;
                case DISCONNECTED:
                    if (!mFirstDisconnectReceived) {
                        mFirstDisconnectReceived = true;
                        Log.w(TAG, "Ignoring first WiFi DISCONNECTED state.");
                    }
                    else {
                        // turn off WiFi
                        WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
                        wifiManager.setWifiEnabled(false);
                        // give up trying
                        Log.e(TAG, "WiFi disconnected, cannot proceed to dropbox authentication");
                        mHandler.sendEmptyMessage(NETWORK_SETUP_FAILED_MESSAGE);
                    }
                    break;
                case DISCONNECTING:
                case SUSPENDED:
                case UNKNOWN:
                    // turn off WiFi
                    WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
                    wifiManager.setWifiEnabled(false);
                    // give up trying
                    Log.e(TAG, "WiFi disconnected, cannot proceed to dropbox authentication");
                    mHandler.sendEmptyMessage(NETWORK_SETUP_FAILED_MESSAGE);
                    break;
            }
        }
    };

    private DropboxAPI<AndroidAuthSession> mDBApi;

    private int getDropBoxRequest() {

        Intent i = getIntent();
        int request = i.getIntExtra(DROP_BOX_REQUEST_FIELD, DROPBOX_AUTH_REQUEST_CODE);

        return request;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dropbox_registration);

        int requestType = getDropBoxRequest();

        switch (requestType) {
            case DROPBOX_AUTH_REQUEST_CODE:
                this.setTitle(R.string.dropbox_oauth_title);
                findViewById(R.id.upload_progress).setVisibility(View.GONE);
                findViewById(R.id.upload_progress_display).setVisibility(View.GONE);
                break;
            case DROPBOX_UPLOAD_REQUEST_CODE:
                this.setTitle(R.string.dropbox_upload_title);
                findViewById(R.id.upload_progress).setVisibility(View.GONE);
                findViewById(R.id.upload_progress_display).setVisibility(View.GONE);
                break;
            default:
                break;
        }

        mHandler = new Handler(this);

        if (isNetworkConnected()) {
            beginDropboxRequest();
        }
        else
        {

            // register for network broadcast
            registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

            // turn on WiFi if not already
            WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
            switch (wifiManager.getWifiState()) {
                case WifiManager.WIFI_STATE_DISABLED:
                case WifiManager.WIFI_STATE_DISABLING:
                    wifiManager.setWifiEnabled(true);
                    mState = DropBoxState.WAIT_NETWORK;
                    ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_waiting_network);
                    break;

                case WifiManager.WIFI_STATE_ENABLED:
                case WifiManager.WIFI_STATE_ENABLING:
                    mState = DropBoxState.WAIT_NETWORK_TIMER;
                    ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_waiting_network);
                    Log.e(TAG, "WiFi is ON but no active connection!!");
                    // set a timer, if still no wifi, report as fail...
                    mHandler.sendEmptyMessageDelayed(NETWORK_WAIT_TIMER_MESSAGE, NETWORK_WAIT_TIMER_SECONDS * 1000);
                    break;

                case WifiManager.WIFI_STATE_UNKNOWN:
                    wifiManager.setWifiEnabled(true);
                    mState = DropBoxState.WAIT_NETWORK;
                    ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
                    ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_waiting_network);
                    Log.w(TAG, "WiFi state is reported as UNKNOWN!!!");
                    break;
            }
        }

    }


    private void beginDropBoxAuthentication() {

        mState = DropBoxState.AUTHENTICATING;

        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        mDBApi = new DropboxAPI<AndroidAuthSession>(session);
        mDBApi.getSession().startOAuth2Authentication(this);


        ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_dropbox_begin_auth);

    }

    private void beginDropBoxUpload() {

        mState = DropBoxState.UPLOADING;

        ((ProgressBar) findViewById(R.id.reg_wait_network_progress)).setVisibility(View.GONE);
        ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.GONE);
        ((ProgressBar)findViewById(R.id.upload_progress)).setVisibility(View.VISIBLE);
        ((ProgressBar)findViewById(R.id.upload_progress)).setProgress(0);
        ((TextView)findViewById(R.id.upload_progress_display)).setVisibility(View.VISIBLE);
        ((TextView)findViewById(R.id.upload_progress_display)).setText(getString(R.string.progress_dropbox_upload) + ": 0%");


        Intent i = getIntent();
        String filePath = i.getStringExtra(DROP_BOX_UPLOAD_FIELD);

        DropboxUploadTask uploadTask = new DropboxUploadTask();
        uploadTask.execute(filePath);

    }

    @Override
    protected void onStart() {
        super.onStart();

        this.getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                        | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    }


    protected void onResume() {
        super.onResume();


        switch (mState) {
            case IDLE:
                ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.INVISIBLE);
                break;
            case WAIT_NETWORK:
            case WAIT_NETWORK_TIMER:
                ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_waiting_network);
                break;
            case AUTHENTICATING:
                ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_dropbox_begin_auth);
                if (mDBApi.getSession().authenticationSuccessful()) {
                    try {
                        // Required to complete auth, sets the access token on the session
                        mDBApi.getSession().finishAuthentication();
                        String accessToken = mDBApi.getSession().getOAuth2AccessToken();
                        Intent i = new Intent(Intent.ACTION_DEFAULT);
                        i.putExtra(OATH_TOKEN_FIELD, accessToken);
                        setResult(RESULT_OK, i);

                    } catch (IllegalStateException e) {
                        Log.i("DbAuthLog", "Error authenticating", e);
                        setResult(RESULT_CANCELED);

                    }

                }
                else {
                    Log.e(TAG, "DropBox AndroidAuthSession ended unsuccessfully");
                    setResult(RESULT_CANCELED);
                }
                ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_dropbox_auth_end);
                mState = DropBoxState.AUTHENTICATED;
                // turn off wifi
                WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
                wifiManager.setWifiEnabled(false);
                finish();
                break;
            case AUTHENTICATED:
                ((ProgressBar)findViewById(R.id.reg_wait_network_progress)).setVisibility(View.INVISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.reg_status_display)).setText(R.string.progress_dropbox_auth_end);
                break;
            case UPLOADING:
            case UPLOADED:
                findViewById(R.id.reg_wait_network_progress).setVisibility(View.GONE);
                findViewById(R.id.reg_status_display).setVisibility(View.GONE);
                ((ProgressBar)findViewById(R.id.upload_progress)).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.upload_progress_display)).setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mNetworkBroadcastReceiver);

    }


    private class DropboxUploadTask extends AsyncTask<String, Integer, Boolean> {

        DropboxAPI.Entry mResponse = null;

        private ProgressListener mDropboxProgressListener = new ProgressListener() {
            @Override
            public void onProgress(long l, long l1) {
                int percentage = (int)(l * 100L / l1);

                publishProgress(percentage);
            }
        };

        @Override
        protected Boolean doInBackground(String... filePath) {
            Boolean uploadCompleted = false;


            try {
                File f = new File(filePath[0]);
                FileInputStream inputStream = new FileInputStream(f);
                // get current hour
                Calendar cal = Calendar.getInstance();
                int hour = cal.get(Calendar.HOUR_OF_DAY);

                AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
                String oath2Token = MainActivity.loadDropBoxToken(getBaseContext());
                AndroidAuthSession session = new AndroidAuthSession(appKeys, oath2Token);
                mDBApi = new DropboxAPI<AndroidAuthSession>(session);
                mResponse = mDBApi.putFileOverwrite("/MuffinCam/capture-" + hour + ".mp4",
                        inputStream, f.length(), mDropboxProgressListener);

                uploadCompleted = true;
            }
            catch (FileNotFoundException fnfe) {

                Log.e(TAG, "Upload file not found: " + filePath + ", " + fnfe.getMessage());
            }
            catch (java.lang.IllegalArgumentException iae) {
                Log.e(TAG, "Upload file not found exception: " + iae.getMessage());
            }
            catch (DropboxUnlinkedException due) {
                Log.e(TAG, "Dropbox Unlinked Exception caught: " + due.getMessage());
            }
            catch (DropboxFileSizeException dfse) {
                Log.e(TAG, "Dropbox File-Size Exception caught: " + dfse.getMessage());
            }
            catch (DropboxServerException dse) {
                Log.e(TAG, "Dropbox Server Exception caught: " + dse.getMessage());
            }
            catch (DropboxIOException dioe) {
                Log.e(TAG, "Dropbox I/O Exception caught: " + dioe.getMessage());
            }
            catch (DropboxException de) {
                Log.e(TAG, "Dropbox Exception caught: " + de.getMessage());
            }

            return uploadCompleted;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {

            ((ProgressBar)findViewById(R.id.upload_progress)).setProgress(progress[0]);

            ((TextView)findViewById(R.id.upload_progress_display)).setText(getString(R.string.progress_dropbox_upload) + ": " +
                    progress[0] + "%");
        }

        @Override
        protected void onPostExecute(Boolean uploaded) {

            mState = DropBoxState.UPLOADED;

            // turn off wifi
            WifiManager wifiManager = (WifiManager)getSystemService(Context.WIFI_SERVICE);
            wifiManager.setWifiEnabled(false);

            if (uploaded) {
                // send message to main thread with the uploaded file path

                Intent intent = new Intent(MainActivity.VIDEO_UPLOADED);
                intent.putExtra(MainActivity.INTENT_EXTRA_DROPBOX_VIDEOPATH, mResponse.fileName());

                // set result code
                setResult(RESULT_OK, intent);
                ((TextView)findViewById(R.id.upload_progress_display)).setText(R.string.progress_dropbox_upload_finish);
            }
            else {

                Intent intent = new Intent(MainActivity.VIDEO_UPLOAD_FAILED);
                setResult(RESULT_CANCELED, intent);
            }

            // finish activity
            finish();
        }

    }

}

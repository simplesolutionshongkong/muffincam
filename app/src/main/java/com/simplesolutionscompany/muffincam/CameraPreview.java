package com.simplesolutionscompany.muffincam;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Created by f.fong on 10/19/2015.
 */
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    public static final String SURFACE_PREVIEW_READY = "com.simplesolutionscompany.muffincam.SURFACE_PREVIEW_READY";
    public static final String SURFACE_PREVIEW_FAILED = "com.simplesolutionscompany.muffincam.SURFACE_PREVIEW_FAILED";

    private static final String TAG = "CameraPreview";
    private Camera mCamera;

    public CameraPreview(Context context, Camera camera) {
        super(context);

        mCamera = camera;
        // register SurfaceHolder callback
        getHolder().addCallback(this);
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        /* Do nothing, leave starting-preview to surfaceChanged() as it will be called anyway */
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

        // restart preview

        if (surfaceHolder.getSurface() == null) {
            // surface not created yet. nothing to do.
            return;
        }

        try {
            Log.w(TAG, "Surface Change Callback values: " + i + ", " + i1 + ", " + i2);
            mCamera.stopPreview();
            Log.w(TAG, "SurfaceChanged 1");
            mCamera.setDisplayOrientation(90); // CameraActivity orientation is portrait
            //
            // Set Camera Preview Size to avoid Camera Error -1007
            // http://www.hitmaroc.net/170122-9090-mediarecorder-stop-stop-failed-1007.html
            //
            Camera.Parameters parameters = mCamera.getParameters();
            int previewWidth = CameraActivity.RecorderProfile.videoFrameWidth;
            int previewHeight = CameraActivity.RecorderProfile.videoFrameHeight;
            Log.w(TAG, "Setting camera preview size " + previewWidth + " x " + previewHeight);
            parameters.setPreviewSize(previewWidth, previewHeight);
            mCamera.setParameters(parameters);
            mCamera.setPreviewDisplay(surfaceHolder);
            mCamera.startPreview();
            LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getContext());
            lbm.sendBroadcast(new Intent(CameraPreview.SURFACE_PREVIEW_READY));
        }
        catch (IOException ioe) {
            Log.e(TAG, "Connect SurfaceView to Camera Failed: " + ioe.getMessage());
            LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(getContext());
            lbm.sendBroadcast(new Intent(CameraPreview.SURFACE_PREVIEW_FAILED));
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

        // mCamera already released mCamera.stopPreview();
        // notify ...
    }
}